require_relative '../lib/main'

module Helpers
  module Methods
    def linal(&block)
      LinalContext.new.instance_eval(&block)
    end

    def simple(&block)
      SimpleContext.new.instance_eval(&block)
    end
  end

  class LinalContext
    def var(name)
      Linal::Var.new(name)
    end

    def num(n)
      Linal::Num.new(n)
    end

    def sum(*list)
      Linal::Sum.new(list)
    end

    def prod(*list)
      Linal::Product.new(list)
    end
  end

  class SimpleContext
    def sum(*list)
      Simple::Sum.new(list)
    end

    def s(*args)
      Simple::Summand.new(*args)
    end
  end
end

RSpec.configure do |config|
  config.include Helpers::Methods
end