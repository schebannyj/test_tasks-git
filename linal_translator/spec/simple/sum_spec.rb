require_relative '../spec_helper'

describe Simple::Sum do
  let(:sum1){
    simple{ sum(s(1)) }
  }
  let(:sum2){
    simple{ sum(s(5, 'x'), s(2, 'y'), s(1)) }
  }
  let(:sum3){
    simple{ sum(s(5, 'x'), s(-2, 'y'), s(-1)) }
  }
  let(:sum4){
    simple{ sum(s(1, 'x'), s(-1, 'y'), s(1)) }
  }
  let(:sum5){
    simple{ sum(s(-5)) }
  }
  let(:sum6){
    simple{ sum(s(-1, 'x'), s(-1, 'y'), s(1)) }
  }
  let(:sum7){
    simple{ sum(s(-6, 'x'), s(2, 'y')) }
  }

  describe '#to_s' do
    it 'gets pretty view for each part and joins them with "+" sign' do
      expect(sum1.to_s).to eq '1'
      expect(sum2.to_s).to eq '5*x + 2*y + 1'
    end

    it 'joins negative parts with "-" sign' do
      expect(sum3.to_s).to eq '5*x - 2*y - 1'
    end

    it 'skips 1 as coefficient' do
      expect(sum4.to_s).to eq 'x - y + 1'
    end

    it 'prints first negative as is' do
      expect(sum5.to_s).to eq '-5'
      expect(sum6.to_s).to eq '-x - y + 1'
      expect(sum7.to_s).to eq '-6*x + 2*y'
    end
  end

end