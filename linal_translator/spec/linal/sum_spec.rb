require_relative '../spec_helper'

describe Linal::Sum do
  let(:sum1){
    linal{ sum num(3), num(-1), num(5) }
  }
  let(:sum2){
    linal{ 
      sum sum(num(3), num(-1)), 
          sum(num(5), num(1))
    }
  }
  let(:sum3){
    linal{ 
      sum prod(num(3), num(-1)), 
          prod(num(5), num(3))
    }
  }
  let(:sum4){
    linal{ 
      sum var('x'), num(3), var('y'), num(5)
    }
  }
  let(:sum5){
    linal{ 
      sum var('x'), num(3), var('y'), num(5), var('x')
    }
  }
  let(:sum6){
    linal{ 
      sum prod(var('y'), num(4)), 
          num(3), 
          prod(num(2), var('x')), 
          num(5), 
          prod(num(2), var('y')),
          prod(num(7), var('x'))
    }
  }
  let(:sum7){
    linal{ sum var('x'), num(0) }
  }
  let(:sum8){
    linal{ sum num(0), var('y'), var('x') }
  }
  let(:sum9){
    linal{ sum var('x'), num(3), prod(num(-1), var('x')), var('y'), prod(num(-1), var('y'))  }
  }
  let(:sum10){
    linal{ sum var('x'), prod(num(-1), var('x'))  }
  }
  let(:sum11){
    linal {
      sum sum(num(1), var('c')),
          sum(var('a'), var('b'))
    }
  }
  let(:sum12){
    linal {
      sum sum(var('x'), prod(num(-1), var('y'))),
          sum(var('y'), prod(num(-1), var('x')))
    }
  }
  let(:sum13){
    linal{
      sum var('x'), var('y'), var('z')
    }
  }

  describe '#preprocess' do
    describe 'sum of numbers' do
      it 'produces a number with evaluated sum' do
        expect(sum1.preprocess).to eq linal{ num(7) }
      end
    end

    describe 'sum of sums of numbers' do
      it 'produces a number with evaluated sum' do
        expect(sum2.preprocess).to eq linal{ num(8) }
      end
    end

    describe 'sum of products of numbers' do
      it 'produces a number with evaluated sum' do
        expect(sum3.preprocess).to eq linal{ num(12) }
      end
    end

    describe 'sum with vars' do
      it 'orders vars by name and puts evaluated number to the end' do
        expect(sum4.preprocess).to eq linal{ sum var('x'), var('y'), num(8) }
      end

      it 'accumulates vars with same name' do
        expect(sum5.preprocess).to eq linal{ sum prod(num(2),var('x')), var('y'), num(8) }
        expect(sum6.preprocess).to eq linal{ sum prod(num(9),var('x')), prod(num(6), var('y')), num(8) }
      end
    end

    describe 'sum of var already ordered' do
      it 'does not change' do
        expect(sum13.preprocess).to eq sum13
      end
    end

    it 'does not leave ZERO values' do
      expect(sum7.preprocess).to eq linal{ var('x') }
      expect(sum8.preprocess).to eq linal{ sum var('x'), var('y') }
      expect(sum9.preprocess).to eq linal{ num(3) }
    end

    it 'becames ZERO if nothing left' do
      expect(sum10.preprocess).to eq linal{ num(0) }
    end

    it 'extracts inner sums' do
      expect(sum11.preprocess).to eq linal{ sum var('a'), var('b'), var('c'), num(1) }
      expect(sum12.preprocess).to eq linal{ num(0) }
    end
  end
end