require_relative '../spec_helper'

describe Linal::Product do
  let(:prod1){
    linal{ prod num(3), num(-1), num(5) }
  }
  let(:prod2){
    linal{ 
      prod prod(num(3), num(3)), 
           prod(num(5), num(2))
    }
  }
  let(:prod3){
    linal{ 
      prod sum(num(3), num(3)), 
           sum(num(5), num(2))
    }
  }
  let(:prod4){
    linal{ 
      prod num(3), num(5), var('x')
    }
  }
  let(:prod5){
    linal{ 
      prod var('x'), num(3), num(5)
    }
  }
  let(:prod6){
    linal{ 
      prod var('x'), num(3), var('y')
    }
  }
  let(:prod7){
    linal{ 
      prod var('x'), num(0), num(5)
    }
  }

  describe '#preprocess' do
    describe 'product of numbers' do
      it 'produces a number with evaluated product' do
        expect(prod1.preprocess).to eq linal{ num(-15) }
      end
    end

    describe 'product of products of numbers' do
      it 'produces a number with evaluated product' do
        expect(prod2.preprocess).to eq linal{ num(90) }
      end
    end

    describe 'product of sums of numbers' do
      it 'produces a number with evaluated product' do
        expect(prod3.preprocess).to eq linal{ num(42) }
      end
    end

    describe 'product with one variable and numbers' do
      it 'produces a product of one evaluated number and the var' do
        expect(prod4.preprocess).to eq linal{ prod num(15), var('x') }
      end

      it 'moves var to the end' do
        expect(prod5.preprocess).to eq linal{ prod num(15), var('x') }
      end
    end

    describe 'product with many variables' do
      it 'is unprocessable' do
        expect{ prod6.preprocess }.to raise_error Linal::UnprocessableExpression
      end
    end

    describe 'product with zero' do
      it 'produces zero' do
        expect(prod7.preprocess).to eq linal{ num(0) }
      end
    end
  end
end