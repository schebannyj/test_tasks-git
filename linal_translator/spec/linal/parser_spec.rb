require_relative '../spec_helper'

describe Linal::Parser do
  def parse(input)
    described_class.parse input
  end

  example 'sum of positives' do
    expect(parse('(+ 1 1)').first).to eq linal{ sum num(1), num(1) }
  end

  example 'sum with negatives' do
    expect(parse('(+ 34 -61)').first).to eq linal{ sum num(34), num(-61) }
  end

  example 'sum with var' do
    expect(parse('(+ 3 k)').first).to eq linal{ sum num(3), var('k') }
  end

  example 'product of numbers' do
    expect(parse('(* 3 5 -7)').first).to eq linal{ prod num(3), num(5), num(-7) }
  end

  example 'composite' do
    src = %(
      ( +
        ( *
          (+ 3 5 -9)
          x
        )
        ( +
          (* 45 y)
          (* 6 z)
          (* 25 t)
        )
        5
        q
      )
    )

    expect(parse(src).first).to eq linal{
      sum prod(
            sum(num(3), num(5), num(-9)),
            var('x')
          ),
          sum(
            prod(num(45), var('y')),
            prod(num(6), var('z')),
            prod(num(25), var('t'))
          ),
          num(5),
          var('q')
    }
  end

  example 'multiple expressions' do
    src = %(
      (+ 1 1)
      (* 3 k)(+ x y)
    )

    expect(parse(src)).to eq [
      linal{ sum num(1), num(1) },
      linal{ prod num(3), var('k') },
      linal{ sum var('x'), var('y') }
    ]
  end

end