require_relative 'spec_helper'

describe 'translation' do
  let(:source){ File.read(File.join("#{File.dirname(__FILE__)}", '..', 'examples', '1.src')) }
  let(:result){ Main::translate(source) }

  expected_result = %w(
    2*a-b+2
    2*x+2*y+2*z
    0
    x+y+2*z
    a+b+c
  )

  expected_result.each_with_index do |res, i|
    describe "#{i+1}" do
      it { 
        expect(result[i].gsub(' ', '')).to eq expected_result[i] 
      }
    end
  end
end