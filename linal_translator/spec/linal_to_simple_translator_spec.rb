require_relative 'spec_helper'

describe LinalToSimpleTranslator do
  let(:linal_num){ linal{ num(23) } }
  let(:linal_var){ linal{ var('k') } }
  let(:linal_prod){ linal{ prod num(9), var('z') } }
  let(:linal_sum){
    Linal::Sum.new([linal_var, linal_prod, linal_num])
  }
  let(:linal_prod_raw1){
    linal{ prod num(9), var('z'), num(2) }
  }
  let(:linal_prod_raw2){
    linal{ prod var('z'), num(2) }
  }
  let(:linal_prod_raw3){
    linal{ prod num(7), num(2) }
  }

  describe 'translation of sum' do
    let(:simple_sum){ LinalToSimpleTranslator.translate(linal_sum) }

    it { expect(simple_sum).to be_kind_of Simple::Sum }

    describe 'summands' do
      let(:summands){ simple_sum.items }

      specify 'number is translated to summand without var' do
        expect(summands[2]).to eq simple{ s(23) }
      end

      specify 'var is translated to summand with number 1 and the var' do
        expect(summands[0]).to eq simple{ s(1, 'k') }
      end

      specify 'product is translated to correspondent summand' do
        expect(summands[1]).to eq simple{ s(9, 'z') }
      end
    end
  end

  describe 'translation of num, var or product' do
    it 'is a sum with one correspondent summand' do
      expect(LinalToSimpleTranslator.translate linal_num).to eq simple{ sum s(23) }
      expect(LinalToSimpleTranslator.translate linal_var).to eq simple{ sum s(1, 'k') }
      expect(LinalToSimpleTranslator.translate linal_prod).to eq simple{ sum s(9, 'z') }
    end
  end

  specify 'unpreprocessed products are not translatable' do
    bad_sum = Linal::Sum.new([linal_prod_raw1, linal_num])
    [linal_prod_raw1, linal_prod_raw2, linal_prod_raw3, bad_sum].each do |linal_bad_expr|
      expect{ LinalToSimpleTranslator.translate linal_bad_expr }.to raise_error LinalToSimpleTranslator::TranslationError
    end
  end
end