class LinalToSimpleTranslator
  class TranslationError < StandardError; end

  class << self
    def translate(linal_expr)
      linal_expr.accept self
    rescue => e
      raise TranslationError, e.message
    end

    def visit_linal_num(num)
      one_summand_sum num
    end

    def visit_linal_var(var)
      one_summand_sum var
    end

    def visit_linal_product(product)
      one_summand_sum product
    end

    def visit_linal_sum(sum)
      Simple::Sum.new(sum.items.map{|i| ToSummandTranslator.visit(i) })
    end

    private

    def one_summand_sum(linal_expr)
      Simple::Sum.new([ToSummandTranslator.visit(linal_expr)])
    end
  end

  class ToSummandTranslator
    class << self
      def visit(expr)
        expr.accept self
      end

      def visit_linal_num(num)
        Simple::Summand.new num.value
      end

      def visit_linal_var(var)
        Simple::Summand.new 1, var.name
      end

      def visit_linal_product(product)
        Simple::Summand.new product.items.first.value, product.items.last.name
      end
    end
  end
end