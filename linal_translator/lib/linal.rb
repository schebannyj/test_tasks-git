module Linal
  class SyntaxError < StandardError; end
  class UnprocessableExpression < StandardError; end
  class ImpossibleOperation < StandardError; end
end

require_relative 'linal/expressions'
require_relative 'linal/parser'