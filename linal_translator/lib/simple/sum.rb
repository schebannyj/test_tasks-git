module Simple
  class Sum
    attr_reader :items

    def initialize(items)
      @items = items.dup.freeze
    end

    def to_s
      [@items.first.to_s].tap do |a|
        (1..@items.size-1).each do |i|
          if @items[i].num >= 0
            a << '+'
            a << @items[i]
          else
            a << '-'
            a << @items[i].abs
          end
        end
      end.join(' ')
    end

    def ==(other)
      self.class === other && @items == other.items
    end
  end
end