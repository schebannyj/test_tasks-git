module Simple
  class Summand
    attr_reader :num, :var

    def initialize(num, var=nil)
      @num, @var = num, var
    end

    def to_s
      if var.nil?
        @num.to_s
      elsif @num == 1
        @var
      elsif @num == -1
        "-#{@var}"
      else
        "#{@num}*#{@var}"
      end
    end

    def abs
      Summand.new @num.abs, @var
    end

    def ==(other)
      self.class === other && @num == other.num && @var == other.var
    end
  end
end