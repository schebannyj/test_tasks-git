module Linal
  class Sum < Expression
    attr_reader :items

    def initialize(items)
      @items = items.dup.freeze
    end

    def preprocess
      @items.inject ZERO do |result, i|
        result = i.preprocess + result
      end
    end

    def accept(visitor)
      visitor.visit_linal_sum(self)
    end

    def ==(other)
      self.class === other && @items == other.items
    end

    def *(expr)
      expr.multiply_by_sum(self)
    end

    def +(expr)
      expr.add_sum(self)
    end

    def multiply_by_num(num)
      Sum.new @items.map{|i| i.multiply_by_num(num)}
    end

    def multiply_by_var(var)
      Sum.new @items.map{|i| i.multiply_by_var(var)}
    end

    def multiply_by_product(product)
      Sum.new @items.map{|i| i.multiply_by_product(product)}
    end

    def multiply_by_sum(sum)
      Sum.new(
        [].tap do |a|
          @items.each do |i|
            sum.items.each do |j|
              a << i*j
            end
          end
        end
      )
    end

    def add_num(num)
      new_items = @items[0..-2]
      i = @items.last.add_num(num)
      new_items << i unless i == ZERO
      create_and_reduce_sum(new_items)
    end

    def add_var(var)
      add_ranked(var)
    end

    def add_product(product)
      if product.rank
        add_ranked(product)
      else
        Sum.new(@items + [product])
      end
    end

    def add_sum(sum)
      sum.items.inject(self){|res, i| i + res }
    end

    private

    def add_ranked(expr)
      new_items = [].tap do |a|
        i = 0
        while i < @items.size && @items[i].rank && @items[i].rank < expr.rank
          a << @items[i]
          i += 1
        end
        if i >= @items.size || @items[i].rank != expr.rank
          a << expr
        else
          x = @items[i] + expr
          a << x unless x == ZERO
          i += 1
        end
        while i < @items.size
          a << @items[i]
          i += 1
        end
      end
      create_and_reduce_sum new_items
    end

    def create_and_reduce_sum(items)
      if items.size == 0
        ZERO
      elsif items.size == 1
        items.first
      else
        Sum.new(items)
      end
    end
  end
end