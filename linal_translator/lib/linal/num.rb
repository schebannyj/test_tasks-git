module Linal
  class Num < Primitive
    attr_reader :value

    def initialize(value)
      @value = value
    end

    def accept(visitor)
      visitor.visit_linal_num(self)
    end

    def ==(other)
      self.class === other && @value == other.value
    end

    def *(expr)
      if @value == 0
        self
      else
        expr.multiply_by_num(self)
      end
    end

    def multiply_by_num(num)
      Num.new(@value*num.value)
    end

    def +(expr)
      if @value == 0
        expr
      else
        expr.add_num(self)
      end
    end

    def add_num(num)
      Num.new(@value+num.value)
    end
  end

  ZERO = Num.new(0)
  ONE = Num.new(1)
end