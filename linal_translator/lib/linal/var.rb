module Linal
  class Var < Primitive
    attr_reader :name

    def initialize(name)
      @name = name
    end

    def ==(other)
      self.class === other && @name == other.name
    end

    def accept(visitor)
      visitor.visit_linal_var(self)
    end

    def *(expr)
      expr.multiply_by_var(self)
    end

    def +(expr)
      expr.add_var(self)
    end

    def multiply_by_num(num)
      Product.new([num, self])
    end

    def multiply_by_var(var)
      raise ImpossibleOperation, 'Var * Var'
    end

    def add_num(num)
      Sum.new([self, num])
    end

    def add_var(var)
      if @name == var.name
        Product.new([Num.new(2), var])
      else
        Sum.new([self, var].sort_by(&:name))
      end
    end

    def rank
      @name
    end
  end
end