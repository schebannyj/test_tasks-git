module Linal
  class Product < Expression
    attr_reader :items

    def initialize(items)
      @items = items.dup.freeze
    end

    def preprocess
      @items.inject ONE do |result, i|
        result = i.preprocess * result
      end
    rescue ImpossibleOperation
      raise UnprocessableExpression, "#{inspect}"
    end

    def accept(visitor)
      visitor.visit_linal_product(self)
    end

    def ==(other)
      self.class === other && @items == other.items
    end

    def *(expr)
      expr.multiply_by_product(self)
    end

    def +(expr)
      expr.add_product(self)
    end

    def multiply_by_num(num)
      Product.new([@items.first.multiply_by_num(num), *@items[1..-1]])
    end

    def multiply_by_var(var)
      Product.new([@items[0..-2], @items.last.multiply_by_var(var)])
    end

    def multiply_by_product(product)
      product.items.inject(self){|res, i| res*i}
    end

    def add_num(num)
      Sum.new([self, num])
    end

    def add_var(var)
      if @items.last == var && @items.size == 2
        c = @items.first.add_num(Num.new(1))
        if c == ZERO
          ZERO
        else
          Product.new([c, var])
        end
      else
        sum_items = [self, var]
        sum_items = sum_items.sort_by(&:rank) if rank
        Sum.new(sum_items)
      end
    end

    def add_product(product)
      if @items.last == product.items.last && @items.size == 2 && product.items.size == 2
        c = @items.first + product.items.first
        if c == ZERO
          ZERO
        else
          Product.new([c, @items.last])
        end
      else
        sum_items = [self, product]
        sum_items = sum_items.sort_by(&:rank) if rank && product.rank
        Sum.new(sum_items)
      end
    end

    def rank
      @items.last.rank
    end
  end
end