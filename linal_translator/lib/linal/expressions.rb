module Linal
  class Expression
    def preprocess
      raise NotImplementedError
    end

    def multiply_by_num(num)
      raise NotImplementedError
    end

    def multiply_by_var(var)
      self * var
    end

    def multiply_by_product(product)
      self * product
    end

    def multiply_by_sum(sum)
      self * sum
    end

    def add_num(num)
      raise NotImplementedError
    end

    def add_var(var)
      self + var
    end

    def add_product(product)
      self + product
    end

    def add_sum(sum)
      self + sum
    end

    def rank
      nil
    end
  end

  class Primitive < Expression
    def primitive?
      true
    end

    def preprocess
      self
    end
  end
end

require_relative 'num'
require_relative 'var'
require_relative 'sum'
require_relative 'product'
