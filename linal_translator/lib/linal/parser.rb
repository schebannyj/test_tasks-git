require 'treetop'
Treetop.load File.join(File.dirname(__FILE__), 'linal_grammar')

module Linal
  class Parser
    class << self
      def parse(source)
        parser = LinalParser.new
        program = parser.parse(source)

        if !program
          raise Linal::SyntaxError, "#{parser.failure_reason}, in line #{parser.failure_line} at #{parser.failure_column}"
        end

        program.expressions
      end
    end
  end
end