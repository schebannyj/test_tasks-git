require_relative 'linal'
require_relative 'simple'
require_relative 'linal_to_simple_translator'

class Main
  class << self
    def translate(linal_source)
      Linal::Parser.parse(linal_source).map{|e|
        LinalToSimpleTranslator.translate(e.preprocess).to_s
      }
    end
  end
end